import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

 export class AtmService 
{
  num_cuenta: string ="";
  contrasena:string = "";
  dinero_monto: number = null;
  sub_mensaje: string = "Ingrese los datos de autenticacion"; 
  di: number=1;
  
 
  actual: string;
  constructor() {} 
  public escribir(n){
    if(this.actual == "1"){
      this.num_cuenta = this.num_cuenta + n.value;
    }
    if(this.actual == "2"){
      this.contrasena = this.contrasena + n.value;
    }
    if(this.actual == "3"){
      if(this.dinero_monto==null){
        this.dinero_monto = n.value;
      }else{
        this.dinero_monto = this.dinero_monto + n.value;
      }
    }
    
  }
  public cancelar()
  {
      this.sub_mensaje= "Operacion Cancelada...";
      this.di=2;
  }
  public borrar()
  {
      this.num_cuenta = "";
      this.contrasena = "";
      this.dinero_monto = null;
  } 
  public aceptar()
  {
    this.sub_mensaje= "Seleccione la operacion que desea realizar";
    this.di=3;
  } 
}


