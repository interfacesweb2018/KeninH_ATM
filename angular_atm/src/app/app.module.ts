import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AtmComponent } from './atm/atm.component';
import { PantallaComponent } from './pantalla/pantalla.component';
import { BotonesComponent } from './botones/botones.component';
import { TecladoComponent } from './teclado/teclado.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    AtmComponent,
    PantallaComponent,
    BotonesComponent,
    TecladoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
