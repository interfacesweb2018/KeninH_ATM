/*import swal from'sweetalert2';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pantalla',
  templateUrl: './pantalla.component.html',
  styleUrls: ['./pantalla.component.css']
})

export class PantallaComponent  {

  saldo=1800;
  monto:number;
  rta:number;
  total: string;
  error:string ="Ingrese un monto mayor a 0";
  error2:string ="Ingrese algun valor en monto ";

  Depositar()
  { 
    if(this.monto==null)
    {
      swal('',this.error2 ,'info');
    }
    else    if(this.monto<=0)
    {
      swal('Operación Invalida', this.error, 'error');

    }
    else
    {
      this.rta=this.saldo+this.monto;
      this.total = "Depósito realizado con exito, su nuevo saldo es: ";
    }
  }

  Retirar()
  {
    if(this.monto==null)
    {
      swal('',this.error2 ,'info');
    }
    else    if(this.monto<=0)
    {
      swal('Operación Invalida', this.error, 'error');

    }
    else
    {
      this.rta=this.saldo-this.monto;
      this.total = "Depósito realizado con exito, su nuevo saldo es: ";
    }
  }

  Consultar()
  {
    this.rta=this.saldo;
    this.total= "Su saldo  es: ";
  } 
}*/
import { Component, OnInit } from '@angular/core';
import { AtmService } from '../services/atm.service';
import { MensajesService } from '../services/mensajes.service';
import { HttpClient } from '@angular/common/http';
import { Clientes } from '../interfaces/clientes';

@Component({
  selector: 'app-pantalla',
  templateUrl: './pantalla.component.html',
  styleUrls: ['./pantalla.component.css']
})
export class PantallaComponent implements OnInit 
{
    clientes:Clientes[];
    url = 'http://localhost:8000/api';
  
  constructor(private atmService: AtmService, private mensajesService: MensajesService, private httpClient: HttpClient) {
      httpClient.get(this.url + '/clientes').subscribe( (data: Clientes[]) => {
      this.clientes = data;
    });
   }

  ngOnInit() {}
  
  public actual(campo)
  {
   this.atmService.actual = campo.name;
  } 
  
}

