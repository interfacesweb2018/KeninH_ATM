import { Component, OnInit } from '@angular/core';
import { AtmService } from '../services/atm.service';
@Component({
  selector: 'app-teclado',
  templateUrl: './teclado.component.html',
  styleUrls: ['./teclado.component.css']
})
export class TecladoComponent implements OnInit {

  constructor(private atmService: AtmService) { }

  ngOnInit() {
  }

  public numeros(n){
  
    this.atmService.escribir(n);
 
  }

}
