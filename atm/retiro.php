<html>
    <head>
        <title>CAJERO LAROYE</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="public/atm.css">
    </head>
    <body>
        <div id="container">
            <div id="botones">
                <div id="b1">
                <a href="deposito.php"><button id="bt1">Deposito</button></a>
                </div>
                <br> 
                <div id="b1">
                <a href="retiro.php"><button id="bt2">Retiro</button></a>
                </div>
                <br><br> 
                <div id="b1">
                <a href="consulta.php"><button id="bt3">Consulta</button></a>
                </div>
            </div>
            <div id="pantalla">
                <center><h4>RETIRO</h4></center>
                    <br><br><br>
                    <form  action="retiro2.php" method="POST">
                        <label for="num_tarjeta">Ingrese el nro de tarjeta</label>
                        <input type="number" name="num_tarjeta" id="inp_1" required>
                        <br><br>
                        <label for="num_cedula">Ingrese el nro de cedula</label>
                        <input type="number" name="num_cedula" id="inp_2" required>
                        <br><br>
                        <label for="monto_retiro">Ingrese el Monto a retirar</label>
                        <input type="number" name="monto_retiro" id="inp_3" required>  
                        <br><br>
                        <input type="submit" value="Retirar" id="btn_accion">
                        <br><br>
                        <h4> Monto Actual:
                            <?php
                                require_once("cajero.php"); 
                                $cajero= new Cajero();
                                echo $cajero->consultar();
                            ?>
                        </h4>
                    </form>
            </div>
            <div id="ranuras">
                <div id="ranura">
                <button id="bt4">INSERTE LA TARJETA</button>
                </div>
                <div id="ranura2">
                <button id="bt5">RETIRE SU DINERO</button>
                </div>
            </div>
        </div>
    </body>
</html>
